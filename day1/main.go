package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatalf("error reading file: %v", err)
	}

	var total float64
	s := bufio.NewScanner(f)
	for s.Scan() {
		n, err := strconv.Atoi(s.Text())
		if err != nil {
			log.Fatalf("error Atoi: %v", err)
		}

		total += calculateFuel(float64(n))
	}

	if err := s.Err(); err != nil {
		log.Fatalf("error scanning: %v", err)
	}

	fmt.Printf("solution: %d\n", int(total))
}

func calculateFuel(moduleMass float64) float64 {
	n := math.Floor(float64(moduleMass)/3) - 2
	if n <= 0 {
		return 0
	}
	return n + calculateFuel(n)
}
