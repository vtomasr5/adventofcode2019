package main

import "testing"

func TestCalculateFuel(t *testing.T) {
	tt := []struct {
		input    float64
		expected float64
	}{
		{14, 2},
		{1969, 966},
		{100756, 50346},
	}

	for _, tc := range tt {
		r := calculateFuel(tc.input)
		if r != tc.expected {
			t.Fatalf("expected %v, got %v", tc.expected, tc.input)
		}
	}
}
