package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	data, err := readInput()
	if err != nil {
		log.Fatalf("could not read the program: %v\n", err)
	}

	// convert []string to []int
	splittedData := strings.Split(data, ",")
	input := make([]int, len(splittedData))
	for i, v := range splittedData {
		n, err := strconv.Atoi(v)
		if err != nil {
			log.Fatalf("cannot Atoi: %v", err)
		}
		input[i] = n
	}

	for i := 0; i <= 99; i++ {
		for j := 0; j <= 99; j++ {
			// set noun and verb
			input[1] = i
			input[2] = j

			// reset memory
			numbers := make([]int, len(input))
			copy(numbers, input)

			result := parseData(numbers)
			if result[0] == 19690720 {
				fmt.Println(100*i + j)
				return
			}
		}
	}
}

func parseData(data []int) []int {
	for i := 0; i < len(data); i += 4 {
		if data[i] == 99 { // halt
			return data
		} else if data[i] == 1 { // add
			data[data[i+3]] = data[data[i+1]] + data[data[i+2]]
		} else if data[i] == 2 { // multiply
			data[data[i+3]] = data[data[i+1]] * data[data[i+2]]
		} else {
			return nil
		}
	}
	return data
}

func readInput() (string, error) {
	f, err := os.Open("input.txt")
	if err != nil {
		return "", err
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	var rawProgram string
	for s.Scan() {
		rawProgram = s.Text()
	}

	if err := s.Err(); err != nil {
		return "", err
	}

	return rawProgram, nil
}
